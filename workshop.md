---
marp: true
paginate: true
---

![bg left](assets/kubernetes.png)

# Základy kubernetes

Ondřej Vlk

---

![bg left](assets/qr.png)

# [bit.ly/49f3fdV](https://bit.ly/49f3fdV)

---

![bg cover](assets/containers.jpeg)

---

# Co je to kontejner

- izolace na úrovni OS -> netřeba pouštět VM
- způsob zabalení aplikace včetně všech jejích závislostí a konfigurace
- zabalený souborový systém + metadata
- aplikace se chová všude stejně, nezáleží na prostředí -> řeší "works on my
  machine"
- jednoduchá škálování, distribuce a nasazení

---

![bg contain](assets/containers-vs-virtual-machines.jpg)

---

# Trocha historie

- **BSD jails** - pouze BSD systémy, oddělení FS, sítě a procesů (1999)
- **OpenVZ** - upravený linux kernel (2005)
- **LXC** - využívá "moderní" izolační featury kernelu (2008)
- **Docker** - původně wrapper nad LXC (2013)
- **OCI** - Open Container Initiative (2014)
  - vytvoření standardu pro kontejnerizaci (cíl nebýt závislý na jedné platformě
    Docker)

---

<!--
_backgroundColor: #1D63ED
_color: #FFFFFF
-->

![bg right:30% contain](assets/docker.png)

# Docker

- jeden z nejpoužívanějších
- umí spouštět, spravovat, vytvářet i publikovat kontejnery
- daemon + CLI/GUI
- registry [hub.docker.com](https://hub.docker.com/)
  - omezení na stahování z registry

---

<!--
_backgroundColor: #000000
_color: #FFFFFF
-->

![bg left:30% fit](assets/k8s-logo.png)

# Kubernetes - co to, proč to?

- orchestrátor kontejnerů
- deklarativní popis běhu aplikace (yaml/json)
- ideální platforma pro běh microservices
- vznikl v Google po vzoru Borgu

---

# Architektura

- control plane (master nodes)
  - etcd
  - API server
  - scheduler
  - controller manager
- data plane (worker nodes)
  - kubelet
  - kube-proxy
  - container runtime

---

![bg fit](assets/architecture.png)

---

# [Pod](https://kubernetes.io/docs/concepts/workloads/pods/)

- základní stavební prvek k8s
- jeden nebo více kontejnerů
- sdílí síť a storage a pid

---

# [Service](https://kubernetes.io/docs/concepts/services-networking/service/)

- zpřístupňuje aplikaci uvnitř clusteru
- přiřazuje IP adresu a port
- load balancing
- DNS záznam
- více typů, nás zajímá pouze ClusterIP

---

# [Namespace](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/)

- logické oddělení clusteru
- síťové politiky, kvóty, ...

---

# Kubectl

```bash
# vytvoření objektu
kubectl run <name> --image=<image> [-n <namespace>] # pouze pro pody
kubectl create [-f <file>] <resource> [<name>] [-n <namespace>]
# aktualizace objektu
kubectl apply [-f <file>] <resource> [<name>] [-n <namespace>]
# získání informací o objektu
kubectl get <resource> [<name>] [-n <namespace>]
kubectl describe <resource> [<name>] [-n <namespace>]
# připojení do kontejneru
kubectl exec -it <pod> [-c <container>] [-n <namespace>] -- <command>
# smazání objektu
kubectl delete <resource> [<name>] [-n <namespace>]
# editace objektu
kubectl edit <resource> [<name>] [-n <namespace>]
# logy
kubectl logs <pod> [-c <container>] [-n <namespace>]
# port forward
kubectl port-forward <pod> <local_port>:<remote_port> [-n <namespace>]
```

---

# Cvičení #1

- zjistěte, jaké jsou v clusteru `nodes`
- zjistěte, jaké jsou v clusteru `namespaces`
- jaké pody běží v namespace `ingress-nginx`?
- kolik celkem běží podů v clusteru?
- zjistěte bližší informace o podech v namespace `ingress-nginx` (včetně logů)

---

# Cvičení #2

- v namespace `default` pusťte pod s názvem `nginx` a image `nginx`
  poslouchající na portu 80
  - port podu 80 zpřístupněte na vašem pc na portu 8080 pomocí
    `kubectl port-forward` a vyzkoušejte v prohlížeči
- zjistěte IP adresu spuštěného podu
- spusťte druhý pod s libovolným názvem a image `nginx`
  - pomocí `kubectl exec` vyzkoušejte `curl <ip_adresa_prvniho_podu>:80`
- vytvořené pody po sobě ukliďte

---

# Objekty

- [api reference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/)

```yaml
apiVersion: <group>/<version>
kind: <resource_name>
metadata:
  name: <name>
  labels:
    <label_name>: <label_value>
  annotations:

    <annotation_name>: <annotation_value>
spec: {}
status: {}
```

- základní jdou generovat pomocí
  `kubectl create <resource> <name> --dry-run=client -o yaml`
  `kubectl run <name> --image <image> --dry-run=client -o yaml` # pro pody

---

# Cvičení #3

- podívejte se jak vypadá základní definice podu a service s použitím
  `kubectl create/run`
- ručně vytvořte definici podu z předchozího cvičení:
  - název `nginx`
  - image `nginx`
  - port 80
- aplikujte vytvořený manifest

---

# [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)

- spouští více instancí podu
- umožňuje rolling update/rollback
- ⚠️ `spec.selector` musí odpovídat `spec.template.metadata.labels`

---

# Cvičení #4

- vytvořte deployment s názvem `nginx` a image `nginx` poslouchající na portu 80
- aplikujte vytvoření objekt do k8s
- zjistěte, jaké pody běží v namespace `default`
- zjistěte, jaké pody běží v namespace `default` a mají label `app=nginx`
- změňte počet replik deploymentu a sledujte co se děje
  - `kubectl scale` nebo `kubectl edit` - vyzkoušejte oboje
  - `kubectl get pod --watch` -- `--watch` streamuje změny

* příkazem `kubectl edit` změňte tag u image `nginx` a sledujte co se děje
* příkazem `kubectl rollout undo` se vraťte na předchozí verzi

---

# Cvičení #5

- pro deployment vytvořte svc typu `ClusterIP` s názvem `nginx` poslouchající a
  směřující na port 80
- zobrazte si informace o svc (`get` i `describe`) - zjistěte IP adresu a její
  endpointy
- spusťe si pod s libovolným jménem a image `nginx` a v něm si vyzkoušejte
  - `curl nginx` - jdeme přes DNS svc nginx
  - `curl <ip_adresa_svc>:80` - jdeme přes IP adresu svc nginx
- totéž proveďte, ale z nového namespace `frontend`, dns svc bude mít tvar
  `<svc_name>.<namespace>`

---

# Přístup zvenku

- vice možností, záleží na prostředí
- NodePort - přes port na všech nodů
- LoadBalancer - přes externí load balancer
- Ingress - přes externí load balancer, ale s možností routovat na základě URL

---

# [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/)

- přesměrování na základě URL
- vyžaduje Ingress Controller
- vytváří se objekt `Ingress`

---

# Cvičení #6

- vytvořte ingress pro svc `nginx` s cestou `/` a přesměrujte na port 80
- vyzkoušejte pomocí prohlížeče - `http://localhost/`

---

# Konfigurace aplikací

- [ConfigMap](https://kubernetes.io/docs/concepts/configuration/configmap/)
- [Secret](https://kubernetes.io/docs/concepts/configuration/secret/)
- mount konfigurace přímo do kontejneru
  - je třeba vytvořit v podu volume - [secret](https://kubernetes.io/docs/concepts/configuration/secret/#use-case-dotfiles-in-a-secret-volume), [configMap](https://kubernetes.io/docs/concepts/storage/volumes/#configmap)
  - volume je třeba mountnout do kontejneru - [volumeMount](https://kubernetes.io/docs/tasks/configure-pod-container/configure-volume-storage/#configure-a-volume-for-a-pod)
- pomocí proměnné prostředí
  - [secret](https://kubernetes.io/docs/tasks/inject-data-application/distribute-credentials-secure/#define-a-container-environment-variable-with-data-from-a-single-secret)
  - [configMap](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/#define-container-environment-variables-using-configmap-data)

---

# Cvičení #7

- vytvořte configmapu s názvem `nginx-config` a obsahem z
  nginx/files/default.conf.template
  - upravte váš deployment tak, aby se configmapa mountla do
    `/etc/nginx/templates/`
- vytvořte configmapu s názvem `nginx-html` a obsahem z `nginx/files/index.html`
  - upravte váš deployment tak, aby se configmapa mountla do `/var/www/html/`

---

# Bonus - WORDPRESS

- vytvořte secret `database`, který bude obsahovat hodnoty:
  - `username` = `root`, `password` = vaše heslo, `database` = `wordpress`
- nastudujte si na jaké proměnné prostředí reaguje image
  [mysql](https://hub.docker.com/_/mysql) a
  [wordpress](https://hub.docker.com/_/wordpress)
- vytvořte deployment `mysql` s 1 replikou a env proměnnými ze secretu
  `database` (použijte `MYSQL_ROOT_PASSWORD` a `MYSQL_DATABASE`), port 3306
- vytvořte svc mysql, který bude směřovat na vytvořený deployment
- vytvořte deployment `wordpress` se 1 replikou a env promennými ze secretu
  `database`, port 80
- vytvořte svc wordpress, který bude směřovat na vytvořený deployment
- vytvořte ingress, který bude směřovat na svc wordpress s cestou `/` a hostem
  `localhost`
