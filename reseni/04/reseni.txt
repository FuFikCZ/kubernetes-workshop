kubectl apply -f nginx-deployment.yaml
kubectl get pod
kubectl get pod --selector app=nginx
kubectl scale deployment nginx --replicas 3
kubectl get pod --watch